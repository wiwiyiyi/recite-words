package swu.cx.roomwords

import android.content.Context

class SharedPreferenceUtil private constructor(){
    private val VIEW_TYPE_SHP = "view_type_shp"
    private val IS_USING_CARD_VIEW = "is_using_card_view"
    companion object{
        private lateinit var mContext:Context
        private var instance:SharedPreferenceUtil?=null
        fun getInstance(context: Context):SharedPreferenceUtil{
            mContext = context
            if (instance==null){
                synchronized(this){
                    if (instance==null){
                        instance = SharedPreferenceUtil()
                    }
                }
            }
            return instance!!
        }
    }
    fun saveData(isUsingCardView:Boolean){
        val shp = mContext.getSharedPreferences(VIEW_TYPE_SHP,Context.MODE_PRIVATE)
       shp.edit().apply {
           putBoolean(IS_USING_CARD_VIEW,isUsingCardView)
           apply()
       }
    }
    fun getData()=mContext.getSharedPreferences(VIEW_TYPE_SHP,Context.MODE_PRIVATE).getBoolean(IS_USING_CARD_VIEW,false)

}