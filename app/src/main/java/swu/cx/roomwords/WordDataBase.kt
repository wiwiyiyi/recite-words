package swu.cx.roomwords

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = [Word::class],version = 1,exportSchema = false)
abstract class WordDataBase:RoomDatabase() {
    companion object{
        var instance:WordDataBase ?= null
        fun getWordDataBaseInstance(context: Context) = instance?: synchronized(this){
            instance?:Room.databaseBuilder(context.applicationContext,WordDataBase::class.java,"words_dataBase").build().also { instance = it }
        }
    }
    abstract fun getWordDao():WordDao
}