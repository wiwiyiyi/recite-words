package swu.cx.roomwords

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class WordsDialog(private val mContext: Context, private val viewModel: MyViewModel):Dialog(mContext,R.style.WordsDialog) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = LayoutInflater.from(mContext).inflate(R.layout.words_add_dialog_layout,null,false)
        setContentView(view)
        setCancelable(false)
        val lp = window?.attributes
        lp?.width = dpTopx(300)
        lp?.height = dpTopx(400)
        lp?.x = dpTopx(0)
        lp?.y = dpTopx(-50)
        window?.attributes = lp


        val cancelBtn = view.findViewById<Button>(R.id.cancelBtn)
        val addBtn = view.findViewById<Button>(R.id.addBtn)
        val inputWords = view.findViewById<EditText>(R.id.inputWords)
        val inputMeanings = view.findViewById<EditText>(R.id.inputMeanings)

        val wordsTextWatcher = object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val english = inputWords.text.toString().trim()
                val chinese = inputMeanings.text.toString().trim()
                addBtn.isEnabled  = english.isNotEmpty() && chinese.isNotEmpty()
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        }
        showKeyBoard(inputWords)
        cancelBtn.setOnClickListener {
            dismiss()
        }
        addBtn.setOnClickListener {
            val english = inputWords.text.toString().trim()
            val chinese = inputMeanings.text.toString().trim()
            viewModel.insert(Word(english,chinese))
            hideKeyBoard(it)
            Toast.makeText(context,"添加成功!",Toast.LENGTH_SHORT).show()
            dismiss()
        }
        inputWords.addTextChangedListener(wordsTextWatcher)
        inputMeanings.addTextChangedListener(wordsTextWatcher)
    }
    private fun dpTopx(dp:Int) = (context.resources.displayMetrics.density*dp).toInt()
    private fun showKeyBoard(editText: EditText){
        editText.requestFocus()
        val inputMethodManager = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(editText,0)

    }
    private fun hideKeyBoard(view: View){
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken,0)

    }
}