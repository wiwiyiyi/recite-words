package swu.cx.roomwords

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Word(
    @ColumnInfo(name = "word") val word:String,
    @ColumnInfo(name = "meaning") val meaning:String,
    @ColumnInfo(name = "chinese_invisible") var chineseInVisible:Boolean = false,
    @PrimaryKey(autoGenerate = true) var id: Int = 0
    )