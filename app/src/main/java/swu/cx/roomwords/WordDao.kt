package swu.cx.roomwords

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WordDao {
    @Insert
    fun insertWords(vararg words:Word)

    @Update
    fun update(vararg words:Word)

    @Delete
    fun deleteWords(vararg words:Word)

    @Query("DELETE FROM WORD")
    fun deleteAllWords()

    @Query("SELECT * FROM WORD ORDER BY ID DESC")
    fun getAllWords():LiveData<List<Word>>

    @Query("SELECT * FROM WORD WHERE word LIKE :pattern ORDER BY ID DESC")
    fun findWordWithPattern(pattern:String):LiveData<List<Word>>

}