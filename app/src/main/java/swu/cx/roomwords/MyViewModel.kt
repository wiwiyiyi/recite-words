package swu.cx.roomwords

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class MyViewModel(application: Application) :AndroidViewModel(application) {
    var allWordsLive = WordsRepository.getWordInstance(application).allLiveWords
    fun insert(vararg words:Word){
        WordsRepository.getWordInstance(getApplication()).insert(*words)
    }
    fun clear(){
        WordsRepository.getWordInstance(getApplication()).clear()
    }
    fun modify(vararg words:Word){
        WordsRepository.getWordInstance(getApplication()).modify(*words)
    }
    fun delete(vararg words:Word){
        WordsRepository.getWordInstance(getApplication()).delete(*words)
    }
    fun findWordWithPattern(pattern:String)=WordsRepository.getWordInstance(getApplication()).findWordWithPattern(pattern)


}